---
title: "Simple Bulk RNA-seq Analysis"
author: "Stefan"
date: "1/15/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Load required R modules
```{r}
# Source local definitions file
source('./locale.R')

# Source helper functions
source('./Simple Utilities.R')
```

## Read in data
Load the data in the form of RangedSummarizedExperiment object, which we have previously prepared.  The loaded RangedSummarizedExperiment object in this case is called 'gse'.

```{r read_data}
# Read in what is effectively an embellished count matrix
load(file = file.path(data_dir, ranged_summarized_experiment))
```

## Read in metadata and augment with a few columns
Add a 'batch' variable and one for (real-valued) time.

```{r read_metadata}
# Read in the metadata
metadata <- read_metadata(metadata_file = file.path(data_dir, metadata_file))

# Automatically relevel the metadata (reference levels should precede alternative levels)
metadata <- relevel_metadata(metadata)

# Carefully define covariates as factor (with appropriate reference levels) and numeric.  Augment the metadata with a few columns
# ... name
name <- as.character(gse$names)

# ... age
age <- as.numeric(metadata$age)

# ... Batch
# ... ... Taehoon's macaques are assigned to batch 1
batch <- rep(1, times = nrow(metadata))

# ... ... Rocky's macaque is aassigned to batch 2
batch[grepl('ZL26', metadata$macaque)] <- 2
batch <- factor(batch) # No need to relevel - will only use fo batch corrections

# ... condition (Note: alreadu automatically releveled)
condition <- factor(metadata$condition)

# ... crispr_status (Note: alreadu automatically releveled)
crispr_status <- factor(metadata$crispr_status)

# ... HSPC VAF
hspc_vaf <- as.numeric(metadata$hspc_vaf)

# ... macaque
macaque <- factor(metadata$macaque) # No need to relevel - will only use fo batch corrections

# ... Group macaque identity and experimental condition into single covariate for poor person's interaction analysis
macaque_condition <- paste(metadata$macaque, metadata$condition, sep = ':')
macaque_condition <- factor(macaque_condition) # If we use this, it will require appropriate releveling later

# ... Group macaque identity and crispr status into single covariate for poor person's interaction analysis
macaque_crispr <- paste(metadata$macaque, metadata$crispr_status, sep = ':')
macaque_crispr <- factor(macaque_crispr) # If we use this, it will require appropriate releveling later

# ... macrophage VAF
macrophage_vaf <- as.numeric(metadata$macrophage_vaf)

# ... sex
sex <- factor(metadata$sex) # No need to relevel - will only use fo batch corrections

# ... (numeric) time
t <- as.numeric(gsub('.{1}$', '', metadata$time))

# ... (ordinal) time
t_class <- factor(ifelse(t < 12, 'Short', 'Long')); t_class <- relevel(t_class, ref = 'Short')

# ... Ccharacter) time
time <- factor(metadata$time) 

# Define a data.frame of new metadata
new_metadata <- data.frame(name = name,
                           age = age,
                           batch = batch,
                           condition = condition,
                           crispr_status = crispr_status,
                           hspc_vaf = hspc_vaf,
                           macaque = macaque,
                           macaque_condition = macaque_condition,
                           macaque_crispr = macaque_crispr,
                           macrophage_vaf = macrophage_vaf,
                           sex = sex,
                           t = t,
                           t_class = t_class,
                           time = time)
rownames(new_metadata) <- new_metadata$name

# Order the metadata in same order as RangedSummarizedExperiment
sample_names <- colData(gse)$names

# FOR LATER: Should add check that metadata contains superset of the samples 
metadata <- new_metadata[sample_names, ]

colData(gse) <- as(object = new_metadata, Class = 'DataFrame')

# Filter out ZL26 - Consider restoring if 
gse <- gse[ ,colData(gse)$macaque != 'ZL26']
metadata <- subset(metadata, subset = macaque != 'ZL26')

head(metadata, n = 50)
```

## Analysis of LPS-Unstimulated Data
For this analysis we will focus on the non-LPS stimulated condition and compare gene expression in CRISPR-edited to non-CRISPR-edited macrophages.

Shrunken log-fold changes are useful for ranking and visualization, without the need for filters on low count genes.  Different shrinkage strategies make different assumptions on the prior distribution.  apeglm assumes that the prior distribution of LFCs is given by Student's t-distribution.
```{r}
# Create the DESeq object for the entire dataset
dds <- DESeqDataSet(gse, design = ~ crispr_status)

# Filter the DESeq object to datasets from the non-LPS-stimulated condition
ctl_samples <- subset(metadata, subset = condition == 'CTL')$name
ctl_dds <- dds[, ctl_samples]

# Re-level the data to ensure comparisons are against the CRISPR-unedited (= 'WT') group
ctl_dds$crispr_status <- relevel(ctl_dds$crispr_status, ref = 'WT')

# Evaluate differential gene expression
ctl_dds <- DESeq2::DESeq(ctl_dds)
res <- results(ctl_dds, contrast = c('crispr_status', 'CRISPR', 'WT'))

# 
res <- lfcShrink(ctl_dds, coef = "crispr_status_CRISPR_vs_WT", type = 'apeglm')

# We clean up the results by ...
# ... creating a dictionary from ENSEMBL genes to gene symbols
ensembl_IDs <- rownames(res)

if(all(grepl('ENSMMUG', ensembl_IDs))) {
  # Only correct the gene IDs if they are ENSEMBL IDs
  symbols <- create_ENS_HGNC_dict(ensembl_IDs)

  # Assign column of ENSEMBL IDs
  res$ensembl_ID <- ensembl_IDs

  # Assign column of gene symbols
   res$symbols <- symbols

  # Assign row names to HGNC synbols
  rownames(res) <- symbols
}

# Adding annotations to the genes from the ENSEMBL database
ann_df <- add_anns(ensembl_ids = ensembl_IDs)
anns <- ann_df$description; names(anns) <- ann_df$ensembl_gene_id
res$descriptions <- anns[ensembl_IDs]

# Print the top results (ordered by adjusted p-value)
head(res, n = 100)
```
## Visualization

```{r}
p_threshold <- 0.1

# Plot a volcano plot
p <- EnhancedVolcano(res, lab = res$symbols, x = 'log2FoldChange', y = 'pvalue') + ggtitle('CTL condition: CRISPR vs WT')
plot(p)

# Order all results by (moderated) log-fold change
res <- res[order(res$lfcSE, decreasing = TRUE, na.last = TRUE), ]
rownames(res) <- res$symbols

# Write out all genes - whether significant or not
write.table(res,
            file = file.path(results_dir, 'Universe_CRISPR_vs_WT_DGE.txt'),
            append = FALSE, quote = FALSE, sep = '\t', col.names = TRUE,
            row.names = TRUE)

# Filter on statistically significant differentially expressed genes
sig_res <- subset(res, padj < p_threshold)
rownames(sig_res) <- sig_res$symbols

# Write out genes that are statistically significant differentially expressed ordered by moderated log-fold change
write.table(sig_res,
            file = file.path(results_dir, 'Significant_CRISPR_vs_WT_DGE.txt'), append = FALSE, quote = FALSE, sep = '\t', col.names = TRUE,
            row.names = TRUE)
```
## EdgeR Analysis
We repeat the analysis of differentially expressed genes with EdgeR.

```{r}
# Extract counts and metadata
counts <- counts(ctl_dds)
metadata <- colData(ctl_dds)
  
# Create gene annotations as data.frame object
# ... ENSEMBL IDs
ensembl_ids <- rownames(counts)
  
# ... Entrez IDs
entrez_ids <- as.vector(create_ENS_Entrez_dict(ensembl_IDs = ensembl_ids))
  
# ... HGNC gene symbols
symbols <- as.vector(create_ENS_HGNC_dict(ensembl_IDs = ensembl_ids))
  
# ... ... descriptions
anns <- add_anns(ensembl_ids = ensembl_ids)
  
# ... genes annotation data.frame
genes <- data.frame(ENSEMBL = ensembl_ids, EntrezGene = entrez_ids, Symbol = symbols, description = anns$description)
  
# Create the DGEList object
dgl <- edgeR::DGEList(counts = counts,
                      genes = genes,
                      group = metadata[['crispr_status']])
  
# Compute variance stabilized counts and switch gene identifiers from Entrez IDs to HGNC symbols
vsd <- DESeq2::vst(ctl_dds, blind = TRUE)
rownames(vsd) <- dgl$genes$Symbol
  
# Filter on expression levels
keep <- edgeR::filterByExpr(dgl)
dgl <- dgl[keep, , keep.lib.sizes = FALSE]
  
# Order transcript counts
dgl <- dgl[order(rowSums(dgl$counts)), ]
  
# Eliminate duplicated gene symbols
dgl <- dgl[!duplicated(dgl$genes$Symbol), ]
  
# Recompute library sizes
dgl$samples$lib.size <- colSums(dgl$counts)
  
# Use Entrez Gene IDs as row names
rownames(dgl$counts) <- rownames(dgl$genes) <- dgl$genes$EntrezGene
  
# Normalize
dgl <- edgeR::calcNormFactors(dgl)
  
# Examine relative similarities of samples via MDS plot - NOT SO USEFUL AS CURRENTLY IMPLEMENTED
palette <- colorRampPalette(c('red', 'blue'))
biology <- metadata[['crispr_status']]; names(biology) <- rownames(metadata)
pal <- palette(nlevels(biology)); colours <- pal[biology]
p <- limma::plotMDS(dgl, col = colours)
plot(p)
  
# Define the model matrix
design <- model.matrix(object = ~ crispr_status, data = metadata); rownames(design) <- rownames(metadata)
  
# Estimate dispersion
dgl <- edgeR::estimateDisp(y = dgl, design = design, robust = TRUE)
edgeR::plotBCV(dgl)
  
# Compute log2 counts per million for each gene and sample
logCPM <- edgeR::cpm(y = dgl, normalized.lib.sizes = TRUE, log = TRUE, prior.count = 2)
rownames(logCPM) <- dgl$genes$Symbol
colnames(logCPM) <- rownames(dgl$samples)
  
# Fit GLM
fit <- glmFit(dgl, design)
  
# Conduct liklihood ratio tests - Note: as invoked, this checks for significance of the *last*  coefficient
# [mutliplying 'biological covariate' in the GLM fit]
lrt <- edgeR::glmLRT(fit)
  
# Adjust p-values for multiple comparisons
p_adjust <- p.adjust(p = lrt$table$PValue, method = 'BH')
  
# Summarize likelihood ratio test into data.frame object
summary_lrt <- data.frame(ENSEMBL = lrt$genes$ENSEMBL,
                          symbol = lrt$genes$Symbol,
                          description = lrt$genes$description,
                          AveLogCPM = lrt$AveLogCPM,
                          log2FoldChange = lrt$table$logFC,
                          LR = lrt$table$LR,
                          PValue = lrt$table$PValue,
                          adjustedPValue = p_adjust)

# Write out results of all (unfiltered) likelihood ratio tests - ordered by logfold change
summary_lrt <- summary_lrt[order(summary_lrt$log2FoldChange, decreasing = TRUE, na.last = TRUE), ]
write.table(x = summary_lrt,
            file = file.path(results_dir, 'Universe_CTL_CRISPR_vs_WT_DGE_via_LRT.txt'),
            append = FALSE, col.names = TRUE, quote = FALSE, row.names = FALSE, sep = '\t')
  
# Print table of the top differentially expressed genes - Same as above only ordered by adjusted p-value
top_genes_df <- topTags(object = lrt, n = min(10000, nrow(summary_lrt)), adjust.method = 'BH', sort.by = 'logFC', p.value = 0.1)
write.table(x = top_genes_df,
              file = file.path(results_dir, 'Significant_CTL_CRISPR_vs_WT_DGE_via_LRT.txt'),
              append = FALSE, col.names = TRUE, quote = FALSE, row.names = FALSE, sep = '\t')
  
# Some diganostic plots
# ... volcano plot
p <- EnhancedVolcano(summary_lrt, lab = summary_lrt$symbol, x = 'log2FoldChange', y = 'adjustedPValue')
plot(p)
  
# Plot log-fold change against log-counts per million
limma::plotMD(lrt)
abline(h = c(-1, 1), col = 'blue')
```
## Gene Ontology Analysis

*To do*: Go over the results of the GO, KEGG and Broad analyses and pick out the pertinent genes from gene sets.  Plot heat maps and volcano plots.
```{r}
# Gene ontology analysis
go <- goana(lrt, species = 'Mmu')
top_GO_df <- topGO(go, ont = 'BP', sort = 'up', number = 500, truncate = 100)
write.table(x = top_GO_df,
            file = file.path(results_dir, 'CTL_CRISPR_vs_WT_topGO.txt'),
            append = FALSE, col.names = TRUE, quote = FALSE, row.names = TRUE, sep = '\t')
  
# KEGG pathway enrichment
kegg <- kegga(lrt, species = 'Mmu')
top_KEGG_df <- topKEGG(kegg, sort = 'up', number = 500, truncate = 100)
write.table(x = top_KEGG_df,
            file = file.path(results_dir, 'CTL_CRISPR_vs_WT_topKEGG.txt'),
            append = FALSE, col.names = TRUE, quote = FALSE, row.names = TRUE, sep = '\t')
```
## Genesets from Broad Geneset Lists
```{r}
# Some parameters for this analysis
focused_gs_search_terms <- c('inflamm', 'nlrp')
geneset_list_fns <- list(custom = file.path(data_dir, 'InterestingGenesets.txt'), # If custom first then can use gene names as search terms
                         curated = 'c2.all.v7.2.symbols.gmt',
                         immunologic = 'c7.all.v7.2.symbols.gmt')
geneset_length <- 10
n_plots <- 50

# Read in manually curated gene lists


# Analysis of selected Broad geneset lists:
# Copy the DGEList and switch to HGNC naming - Note Broad genesets are specified either as HGNC symbols or (human) Entrez ID;
# There is no easy translation from macaque Entrez ID to human Entrez ID, so we switch to HGNC symbols
dgl_hgnc <- dgl; rownames(dgl_hgnc$counts) <- rownames(dgl_hgnc$genes) <- dgl_hgnc$genes$Symbol
    
sig_genesets_l <- list()
for(gsl_name in names(geneset_list_fns)) {
  # Toggle between custom geneset and Broad generated
  if(gsl_name == 'custom') {
    # Read the curated geneset list
    other_genesets <- read.table(file = file.path(data_dir, 'InterestingGenesets.txt'),
                                 header = TRUE, sep = '\t', stringsAsFactors = FALSE)
    
    # Reformat into Broad geneset list format
    other_geneset_names <- unique(other_genesets$Pathway)
    geneset_list <- list()
    geneset_list$genesets <- list()
    geneset_list$geneset.names <- character(0)
    geneset_list$geneset.descriptions <- character(0)
    for(geneset_name in geneset_names) {
      cat('Adding', geneset_name, 'to geneset list ...')
      geneset_list$geneset.names <- c(geneset_list$geneset.names, geneset_name)
      geneset_list$genesets <- c(geneset_list$genesets, as.list(subset(other_genesets, subset = Pathway == geneset_name, select = Symbol)))
      names(geneset_list$genesets) <- geneset_list$geneset.names
      geneset_list$geneset.descriptions <- c(geneset_list$geneset.descriptions, geneset_name)
      cat('Done.\n')
    }
    
    focused_gs_search_termss <- c(focused_gs_search_terms, geneset_list$genesets[['OTHER']])
  } else {
    # Read in entire Broad geneset list
    log.msg <- capture.output(
      geneset_list <- GSA::GSA.read.gmt(filename = file.path(broad_geneset_dir, geneset_list_fns[[gsl_name]]))
    )
    
    if(!is.null(geneset_length)) {
      # Filter on basis of size
      gs_sizes <- sapply(geneset_list$genesets, FUN = function(gs){length(gs)})
      
      geneset_list$genesets <- geneset_list$genesets[gs_sizes < geneset_length]
      geneset_list$geneset.names <- geneset_list$geneset.names[gs_sizes < geneset_length]
      geneset_list$geneset.descriptions <- geneset_list$geneset.descriptions[gs_sizes < geneset_length]
    }
    
    if(!is.null(focused_gs_search_terms) && gsl_name != 'custom') {
      # Filter on basis of any symbols in genesets matching the search terms
      gsl_by_symbol <- query_genesetlist_symbols(the_geneset = focused_gs_search_terms,
                                                 geneset_list = geneset_list,
                                                 threshold = 1)
      
      # Filter on basis of descriptions of genesets matching the search terms
      gsl_by_description <- query_genesetlist_description(patterns = focused_gs_search_terms,
                                                          geneset_list = geneset_list,
                                                          threshold = 1)
      
      # Take union of these genesets
      gsl <- list()
      gsl$genesets <- c(gsl_by_symbol$genesets, gsl_by_description$genesets)
      gsl$geneset.names <- c(gsl_by_symbol$geneset.names, gsl_by_description$geneset.names)
      gsl$geneset.descriptions <- c(gsl_by_symbol$geneset.descriptions, gsl_by_description$geneset.description)
      
      geneset_list <- gsl
      
      # Append search terms to gsl_name for file naming
      gsl_fn <- paste(gsl_name, paste(focused_gs_search_terms, collapse = '.'), sep = '_')
    } else {
      gsl_fn <- gsl_name
    }
  }
      
  m <- length(geneset_list$geneset.names)
  if(m) {
    boolean_gs_list <- lapply(geneset_list$genesets, FUN = function(geneset, ranked_genes = ranked_genes){ranked_genes %in% geneset},
                              ranked_genes = rownames(dgl_hgnc))
    names(boolean_gs_list) <- geneset_list$geneset.names
        
    #  f <- mroast(y = dgl, index = restructured_gs_list, design = design) # too resource intensive, so use fry
    f <- fry(y = dgl, index = boolean_gs_list, design = design)
        
    # Adjust the p-value for multiple testing and add the adjusted p-value to the geneset significance testing results data.frame
    p_adjust <- p.adjust(p = f$PValue, method = 'BH')
    f$adjustedPValue <- p_adjust
        
    # Sort genesets by adjusted p-value
    # ... geneset significance testing results
    o <- order(f$adjustedPValue, decreasing = FALSE)
    f <- f[o, ]
        
    # ... boolean geneset list
    o <- match(names(boolean_gs_list), rownames(f)); o <- o[!is.na(o)]
    boolean_gs_list <- boolean_gs_list[o]
        
    # ... geneset list
    o <- match(geneset_list$geneset.names, rownames(f))
    gsl <- list()
    gsl$genesets <- geneset_list$genesets[o]
    gsl$geneset.names <- geneset_list$geneset_names[0]
    gsl$geneset.descriptions <- geneset_list$geneset.descriptions[o]
  }
      
  # Limit number of plots to number of genesets tested
  n <- min(n_plots, m)
      
  if(n) {
    # Display plots for sigbnificant gene sets ...
    for(i in 1:n) {
      titl <- names(boolean_gs_list)[[i]]
      cat(titl,  ' ...')
          
      if(sum(boolean_gs_list[[i]]) > 1) {
        # ... Plot barcode plot
        cat('barcode plot ... ' )
        barcodeplot(statistics = lrt$table$logFC, index = boolean_gs_list[[i]])
        title(main = titl)
            
        # ... Summarize statistics for this plot
        cat('camera ... ' )
        camera(y = dgl, index = boolean_gs_list[[i]], design = design)
            
        # ... Plot the log2-fold change versus mean log2 counts per million, *highlighting* genes in this geneset
        cat('log2FC 1 ... ' )
        with(lrt$table, plot(logCPM, logFC, pch = 16, cex = 0.2))
        with(lrt$table, points(logCPM[boolean_gs_list[[i]]], logFC[boolean_gs_list[[i]]], pch = 16, col = 'red'))
        title(main = titl)
            
        # ... Plot the log2-fold change versus mean log2 counts per million
        gs_summary_lrt <- summary_lrt
        In_GS <- gs_summary_lrt$symbol %in% gsl$genesets[[i]]
        gs_summary_lrt$In_GS <- ifelse(In_GS, 'In GS', 'Not in GS')
        gs_summary_lrt$label <- ifelse(In_GS, gs_summary_lrt$symbol, '')
        p <- ggplot2::ggplot(data = gs_summary_lrt, aes(x = AveLogCPM, y = log2FoldChange)) + geom_point(aes(color = In_GS)) +
          geom_text(aes(label = label), size = 3) + xlab('Avg log2 CPM') + ylab('log2 FC') + ggtitle(titl)
        plot(p)
            
        # ... Plot heatmap
        cat('heat map ... ' )
        # ... ... interesting genes
        interesting_genes <- rownames(vsd)[rownames(vsd) %in% gsl$genesets[[i]]]
            
        # ... ... heatmap via pheatmap
        draw_heatmap(interesting_genes,
                      vsd,
                      covariate = 'crispr_status',
                      display.ENSEMBL = FALSE,
                      extreme.range = 0.33,
                      frac.extreme.range = 0.4,
                      show.all_genes = length(interesting_genes) < 50,
                      show.colnames = FALSE)
        title(main = titl)

        # # ... ... heatmap via limma's coolmap
        # gs_logCPM <- logCPM[rownames(logCPM) %in% gsl$genesets[[i]], ]
        # limma::coolmap(x = gs_logCPM,
        #                cluster.by = 'de pattern',
        #                linkage.col = 'median',
        #                show.dendrogram = 'both')
        # title(main = titl)
            
        # ... Plot (enhanced) volcano plot
        cat('volcano plot\n' )
        gs_summary_lrt <- subset(gs_summary_lrt, subset = In_GS == 'In GS')
        p <- EnhancedVolcano(toptable = gs_summary_lrt,
                              lab = gs_summary_lrt$symbol,
                              x = 'log2FoldChange',
                              y = 'adjustedPValue',
                              title = titl)
        plot(p)
      }
    }
  }
      
  # Write the table
  write.table(x = f,
              file = file.path(results_dir, paste0('CTL_CRISPR_vs_WT_', gsl_fn, '.txt')),
              append = FALSE, col.names = TRUE, quote = FALSE, row.names = TRUE, sep = '\t')
      
  # Save the list
  sig_genesets_l[[gsl_name]] <- f
}
```
```{r}


# Filter on basis of any symbols in genesets matching the search terms
gsl_by_symbol <- query_genesetlist_symbols(the_geneset = focused_gs_search_terms,
                                               geneset_list = geneset_list,
                                               threshold = 1)
        
    # Filter on basis of descriptions of genesets matching the search terms
    gsl_by_description <- query_genesetlist_description(patterns = focused_gs_search_terms,
                                                        geneset_list = geneset_list,
                                                        threshold = 1)
        
    # Take union of these genesets
    gsl <- list()
    gsl$genesets <- c(gsl_by_symbol$genesets, gsl_by_description$genesets)
    gsl$geneset.names <- c(gsl_by_symbol$geneset.names, gsl_by_description$geneset.names)
    gsl$geneset.descriptions <- c(gsl_by_symbol$geneset.descriptions, gsl_by_description$geneset.description)
        
    geneset_list <- gsl
        
    # Append search terms to gsl_name for file naming
    gsl_fn <- paste(gsl_name, paste(focused_gs_search_terms, collapse = '.'), sep = '_')
  } else {
    gsl_fn <- gsl_name
  }
      
  m <- length(geneset_list$geneset.names)
  if(m) {
    boolean_gs_list <- lapply(geneset_list$genesets, FUN = function(geneset, ranked_genes = ranked_genes){ranked_genes %in% geneset},
                              ranked_genes = rownames(dgl_hgnc))
    names(boolean_gs_list) <- geneset_list$geneset.names
        
    #  f <- mroast(y = dgl, index = restructured_gs_list, design = design) # too resource intensive, so use fry
    f <- fry(y = dgl, index = boolean_gs_list, design = design)
        
    # Adjust the p-value for multiple testing and add the adjusted p-value to the geneset significance testing results data.frame
    p_adjust <- p.adjust(p = f$PValue, method = 'BH')
    f$adjustedPValue <- p_adjust
        
    # Sort genesets by adjusted p-value
    # ... geneset significance testing results
    o <- order(f$adjustedPValue, decreasing = FALSE)
    f <- f[o, ]
        
    # ... boolean geneset list
    o <- match(names(boolean_gs_list), rownames(f)); o <- o[!is.na(o)]
    boolean_gs_list <- boolean_gs_list[o]
        
    # ... geneset list
    o <- match(geneset_list$geneset.names, rownames(f))
    gsl <- list()
    gsl$genesets <- geneset_list$genesets[o]
    gsl$geneset.names <- geneset_list$geneset_names[0]
    gsl$geneset.descriptions <- geneset_list$geneset.descriptions[o]
  }
      
  # Limit number of plots to number of genesets tested
  n <- min(n_plots, m)
      
  if(n) {
    # Display plots for sigbnificant gene sets ...
    for(i in 1:n) {
      titl <- names(boolean_gs_list)[[i]]
      cat(titl,  ' ...')
          
      if(sum(boolean_gs_list[[i]]) > 1) {
        # ... Plot barcode plot
        cat('barcode plot ... ' )
        barcodeplot(statistics = lrt$table$logFC, index = boolean_gs_list[[i]])
        title(main = titl)
            
        # ... Summarize statistics for this plot
        cat('camera ... ' )
        camera(y = dgl, index = boolean_gs_list[[i]], design = design)
            
        # ... Plot the log2-fold change versus mean log2 counts per million, *highlighting* genes in this geneset
        cat('log2FC 1 ... ' )
        with(lrt$table, plot(logCPM, logFC, pch = 16, cex = 0.2))
        with(lrt$table, points(logCPM[boolean_gs_list[[i]]], logFC[boolean_gs_list[[i]]], pch = 16, col = 'red'))
        title(main = titl)
            
        # ... Plot the log2-fold change versus mean log2 counts per million
        gs_summary_lrt <- summary_lrt
        In_GS <- gs_summary_lrt$symbol %in% gsl$genesets[[i]]
        gs_summary_lrt$In_GS <- ifelse(In_GS, 'In GS', 'Not in GS')
        gs_summary_lrt$label <- ifelse(In_GS, gs_summary_lrt$symbol, '')
        p <- ggplot2::ggplot(data = gs_summary_lrt, aes(x = AveLogCPM, y = log2FoldChange)) + geom_point(aes(color = In_GS)) +
          geom_text(aes(label = label), size = 3) + xlab('Avg log2 CPM') + ylab('log2 FC') + ggtitle(titl)
        plot(p)
            
        # ... Plot heatmap
        cat('heat map ... ' )
        # ... ... interesting genes
        interesting_genes <- rownames(vsd)[rownames(vsd) %in% gsl$genesets[[i]]]
            
        # ... ... heatmap via pheatmap
        draw_heatmap(interesting_genes,
                      vsd,
                      covariate = 'crispr_status',
                      display.ENSEMBL = FALSE,
                      extreme.range = 0.33,
                      frac.extreme.range = 0.4,
                      show.all_genes = length(interesting_genes) < 50,
                      show.colnames = FALSE)
        title(main = titl)

        # # ... ... heatmap via limma's coolmap
        # gs_logCPM <- logCPM[rownames(logCPM) %in% gsl$genesets[[i]], ]
        # limma::coolmap(x = gs_logCPM,
        #                cluster.by = 'de pattern',
        #                linkage.col = 'median',
        #                show.dendrogram = 'both')
        # title(main = titl)
            
        # ... Plot (enhanced) volcano plot
        cat('volcano plot\n' )
        gs_summary_lrt <- subset(gs_summary_lrt, subset = In_GS == 'In GS')
        p <- EnhancedVolcano(toptable = gs_summary_lrt,
                              lab = gs_summary_lrt$symbol,
                              x = 'log2FoldChange',
                              y = 'adjustedPValue',
                              title = titl)
        plot(p)
      }
    }
  }
      
  # Write the table
  write.table(x = f,
              file = file.path(results_dir, paste0('CTL_CRISPR_vs_WT_', gsl_fn, '.txt')),
              append = FALSE, col.names = TRUE, quote = FALSE, row.names = TRUE, sep = '\t')
      
  # Save the list
  sig_genesets_l[[gsl_name]] <- f
}
```


